LibraryNames libTopEventSelectionTools libTopEventReconstructionTools

### Good Run List
GRLDir  GoodRunsLists
GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20180129/physics_25ns_21.0.19.xml data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml

### Pile-up reweighting tool - Metadata determines which to use

# MC16d configuration
PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.FS.v2/CI.prw.merged.root
PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.AF.v2/CI.prw.merged.root
PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root


BTagCDIPath xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root

### Object container names
ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMPFlowJets_BTagging201810
LargeJetCollectionName AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets 
TauCollectionName TauJets
PhotonCollectionName Photons
TrackJetCollectionName AntiKtVR30Rmax4Rmin02TrackJets
JetGhostTrackDecoName GhostTrack

### Large-R configuration
LargeRJESJMSConfig CombMass
LargeRJetPt 200000
LargeRJetEta 2
LargeJetSubstructure None

### Reclustered jet configuration
UseRCJets True
#UseRCJetSubstructure True
RCJetEta 2.0
UseVarRCJets True
VarRCJetMassScale m_w,m_t
VarRCJetRho 2

### Truth configuration
TruthCollectionName TruthParticles
TruthJetCollectionName AntiKt4TruthWZJets
TruthLargeRJetCollectionName AntiKt10TruthTrimmedPtFrac5SmallR20Jets
TopPartonHistory ttbar
TopParticleLevel True
TruthBlockInfo False
PDFInfo True

### Object loader/ output configuration
ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No


### Systematics configuration
Systematics Nominal
JetUncertainties_NPModel CategoryReduction
LargeRJetUncertainties_NPModel CategoryReduction

### Electron configuration
ElectronID TightLH
ElectronIDLoose LooseAndBLayerLH
ElectronIsolation Gradient
ElectronIsolationLoose None

### Photon configuration
PhotonPt 25000
PhotonEta 2.5
PhotonID Tight
PhotonIDLoose Loose
PhotonIsolation FixedCutTight
PhotonIsolationLoose FixedCutLoose
PhotonUseRadiativeZ False

### Muon configuration
MuonQuality Medium
MuonQualityLoose Medium
MuonIsolation FCTight_FixedRad
MuonIsolationLoose None

### Tau configuration
TauPt 25000
TauJetIDWP Medium
TauJetIDWPLoose Medium
TauEleBDTWP Loose
TauEleBDTWPLoose Loose
TauEleOLR False
TauEleOLRLoose False

# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose Data

# Turn on MetaData to pull IsAFII from metadata
UseAodMetaData True
#IsAFII False

### Boosted jet taggers configuration
BoostedJetTagging JSSWTopTaggerDNN:DNNTaggerTopQuarkInclusive50 JSSWTopTaggerDNN:DNNTaggerTopQuarkInclusive80 JSSWTopTaggerDNN:DNNTaggerTopQuarkContained50 JSSWTopTaggerDNN:DNNTaggerTopQuarkContained80

### B-tagging configuration
BTaggingWP MV2c10:FixedCutBEff_77 MV2c10:Continuous DL1:Continuous DL1r:FixedCutBEff_77
# Example of how to remove systematics from b-tag EV
#BTaggingSystExcludedFromEV FT_EFF_JET_BJES_Response;FT_EFF_JET_EffectiveNP_1;FT_EFF_EG_RESOLUTION_ALL

# Saving bootstrapping weights
SaveBootstrapWeights False
NumberOfBootstrapReplicas 100

#NEvents 500

### Global lepton trigger scale factor example
UseGlobalLeptonTriggerSF True
GlobalTriggers 2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50
GlobalTriggersLoose 2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50

########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL

########################
### definition of the data periods
########################

SUB period_2015
RUN_NUMBER >= 276262
RUN_NUMBER <= 284484

SUB period_2016
RUN_NUMBER >= 296939
RUN_NUMBER <= 311481

SUB period_2017
RUN_NUMBER >= 324320
RUN_NUMBER <= 341649

SUB period_2018
RUN_NUMBER >= 348197


########################
### lepton trigger and offline cuts for reco-level selections
########################

SUB EL_2015
. BASIC
. period_2015
GTRIGDEC
EL_N 25000 >= 1

SUB EL_2016
. BASIC
. period_2016
GTRIGDEC
EL_N 27000 >= 1

SUB EL_2017
. BASIC
. period_2017
GTRIGDEC
EL_N 27000 >= 1

SUB EL_2018
. BASIC
. period_2018
GTRIGDEC
EL_N 27000 >= 1

SUB MU_2015
. BASIC
. period_2015
GTRIGDEC
MU_N 25000 >= 1

SUB MU_2016
. BASIC
. period_2016
GTRIGDEC
MU_N 27000 >= 1

SUB MU_2017
. BASIC
. period_2017
GTRIGDEC
MU_N 27000 >= 1

SUB MU_2018
. BASIC
. period_2018
GTRIGDEC
MU_N 27000 >= 1

SUB EM_2015
. BASIC
. period_2015
GTRIGDEC
EL_N_OR_MU_N 25000 >= 1

SUB EM_2016
. BASIC
. period_2016
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2017
. BASIC
. period_2017
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2018
. BASIC
. period_2018
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

########################
### e+jets selections
########################

SUB ejets_basic
EL_N 25000 == 1
MU_N 25000 == 0
GTRIGMATCH
#EMU_OVERLAP
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 >= 2
JET_N 25000 >= 3
JET_N 25000 >= 4
MET > 30000
MWT > 30000
#RECO::KLFITTERRUN kElectron KLFitterBTaggingWP:MV2c10:FixedCutBEff_77
#KLFITTER > -50.0
EXAMPLEPLOTS
#PRINT
NOBADMUON

SELECTION ejets_2015
. EL_2015
. ejets_basic
SAVE

SELECTION ejets_2016
. EL_2016
. ejets_basic
SAVE

SELECTION ejets_2017
. EL_2017
. ejets_basic
SAVE

SELECTION ejets_2018
. EL_2018
. ejets_basic
SAVE

SELECTION ejets_MV2c10_2015
. EL_2015
. ejets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_MV2c10_2016
. EL_2016
. ejets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_MV2c10_2017
. EL_2017
. ejets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_MV2c10_2018
. EL_2018
. ejets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_DL1r_2015
. EL_2015
. ejets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_DL1r_2016
. EL_2016
. ejets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_DL1r_2017
. EL_2017
. ejets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_DL1r_2018
. EL_2018
. ejets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

########################################################################
# For testing whether track jet b-tag selection works
# ONLY FOR TESTING, DOES NOT MAKE SENSE TO USE THIS FOR PHYSICS ANALYSIS
# THESE COMBINE TRACK-JET B-TAGGING WITH KLFITTER ON CALO JETS!!!

SELECTION ejets_tjet_MV2c10_2015
. EL_2015
. ejets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_tjet_MV2c10_2016
. EL_2016
. ejets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_tjet_MV2c10_2017
. EL_2017
. ejets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION ejets_tjet_MV2c10_2018
. EL_2018
. ejets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE
########################################################################

SELECTION ejets_particle
PRIVTX
PARTICLE_LEVEL
EL_N 27000 >= 1
. ejets_basic
SAVE

########################
### mu+jets selections
########################

SUB mujets_basic
MU_N 25000 == 1
EL_N 25000 == 0
GTRIGMATCH
#EMU_OVERLAP
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 >= 2
JET_N 25000 >= 3
JET_N 25000 >= 4
MET+MWT > 60000
#RECO::KLFITTERRUN kMuon KLFitterBTaggingWP:MV2c10:FixedCutBEff_77
#KLFITTER > -50.0
EXAMPLEPLOTS
#PRINT
NOBADMUON

SELECTION mujets_2015
. MU_2015
. mujets_basic
SAVE

SELECTION mujets_2016
. MU_2016
. mujets_basic
SAVE

SELECTION mujets_2017
. MU_2017
. mujets_basic
SAVE

SELECTION mujets_2018
. MU_2018
. mujets_basic
SAVE

SELECTION mujets_MV2c10_2015
. MU_2015
. mujets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_MV2c10_2016
. MU_2016
. mujets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_MV2c10_2017
. MU_2017
. mujets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_MV2c10_2018
. MU_2018
. mujets_basic
JET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_DL1r_2015
. MU_2015
. mujets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_DL1r_2016
. MU_2016
. mujets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_DL1r_2017
. MU_2017
. mujets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_DL1r_2018
. MU_2018
. mujets_basic
JET_N_BTAG DL1r:FixedCutBEff_77 >= 1
SAVE

########################################################################
# For testing whether track jet b-tag selection works
# ONLY FOR TESTING, DOES NOT MAKE SENSE TO USE THIS FOR PHYSICS ANALYSIS
# THESE COMBINE TRACK-JET B-TAGGING WITH KLFITTER ON CALO JETS!!!

SELECTION mujets_tjet_MV2c10_2015
. MU_2015
. mujets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_tjet_MV2c10_2016
. MU_2016
. mujets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_tjet_MV2c10_2017
. MU_2017
. mujets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE

SELECTION mujets_tjet_MV2c10_2018
. MU_2018
. mujets_basic
TJET_N_BTAG MV2c10:FixedCutBEff_77 >= 1
SAVE
########################################################################

SELECTION mujets_particle
PRIVTX
PARTICLE_LEVEL
MU_N 27000 >= 1
. mujets_basic
SAVE

########################
### emu selections
########################

SUB emu_basic
EL_N 25000 >= 1
MU_N 25000 >= 1
GTRIGMATCH
#EMU_OVERLAP
JETCLEAN LooseBad
HT > 120000
JET_N 25000 >= 1
JET_N 25000 >= 2
EL_N 25000 == 1
MU_N 25000 == 1
OS
MLL > 15000
#TRUTH_MATCH
#JET_N_BTAG FixedCutBEff_77 >= 1
EXAMPLEPLOTS
NOBADMUON

SELECTION emu_2015
. EM_2015
. emu_basic
SAVE

SELECTION emu_2016
. EM_2016
. emu_basic
SAVE

SELECTION emu_2017
. EM_2017
. emu_basic
SAVE

SELECTION emu_2018
. EM_2018
. emu_basic
SAVE

SELECTION emu_particle
PRIVTX
PARTICLE_LEVEL
EL_N_OR_MU_N 27000 >= 1
. emu_basic
SAVE

########################
### ee selections
########################

SUB ee_basic
EL_N 25000 >= 2
GTRIGMATCH
JETCLEAN LooseBad
MET > 60000
JET_N 25000 >= 1
JET_N 25000 >= 2
EL_N 25000 == 2
MU_N 25000 == 0
OS
MLL > 15000
MLLWIN 80000 100000
#TRUTH_MATCH
#JET_N_BTAG FixedCutBEff_77 >= 1
EXAMPLEPLOTS
#JET_N_BTAG FixedCutBEff_77 > 1
NOBADMUON

SELECTION ee_2015
. EL_2015
. ee_basic
SAVE

SELECTION ee_2016
. EL_2016
. ee_basic
SAVE

SELECTION ee_2017
. EL_2017
. ee_basic
SAVE

SELECTION ee_2018
. EL_2018
. ee_basic
SAVE

SELECTION ee_particle
PRIVTX
PARTICLE_LEVEL
EL_N 27000 >= 1
. ee_basic
SAVE

########################
### mumu selections
########################

SUB mumu_basic
MU_N 25000 >= 2
GTRIGMATCH
#EMU_OVERLAP
JETCLEAN LooseBad
MET > 60000
JET_N 25000 >= 1
JET_N 25000 >= 2
MU_N 25000 == 2
EL_N 25000 == 0
OS
MLL > 15000
MLLWIN 80000 100000
#TRUTH_MATCH
#JET_N_BTAG FixedCutBEff_77 >= 1
EXAMPLEPLOTS
#JET_N_BTAG FixedCutBEff_77 > 1
NOBADMUON

SELECTION mumu_2015
. MU_2015
. mumu_basic
SAVE

SELECTION mumu_2016
. MU_2016
. mumu_basic
SAVE

SELECTION mumu_2017
. MU_2017
. mumu_basic
SAVE

SELECTION mumu_2018
. MU_2018
. mumu_basic
SAVE

SELECTION mumu_particle
PRIVTX
PARTICLE_LEVEL
MU_N 27000 >= 1
. mumu_basic
SAVE

########################
### large-r jet selections
########################

SELECTION large_r_jets
INITIAL
GRL
GOODCALO
PRIVTX
JETCLEAN LooseBad
LJET_N 350000 >=1
SAVE

SELECTION rc_jets
INITIAL
GRL
GOODCALO
PRIVTX
JETCLEAN LooseBad
RCJET_N 350000 >=1
SAVE

SELECTION var_rc_jets
INITIAL
GRL
GOODCALO
PRIVTX
JETCLEAN LooseBad
VRCJET_N 2m_w 300000 >= 1
VRCJET_N 2m_t 350000 >= 1
SAVE
